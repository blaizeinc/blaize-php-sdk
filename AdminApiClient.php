<?php

require_once('./vendor/autoload.php');
require_once('./BlaizeAuthPlugin.php'); 

use Http\Discovery\HttpClientDiscovery;
use Http\Client\Common\HttpClientPool\LeastUsedClientPool;
use Http\Client\Common\PluginClient;

class AdminApiClient {

	public static $POOL_SIZE = 4;
	public $httpClientPool;

	public static function build($accessKey, $secretKey) {

		$client = new AdminApiClient($accessKey, $secretKey);

		return $client;
	}

	private function __construct($accessKey, $secretKey) {

		$this->httpClientPool = new LeastUsedClientPool();
		
		for ($i = 0; $i < AdminApiClient::$POOL_SIZE; $i++) {
			$client = new PluginClient(HttpClientDiscovery::find(), [new BlaizeAuthPlugin($accessKey, $secretKey)]);
			$this->httpClientPool->addHttpClient($client);
		}

	}

}
