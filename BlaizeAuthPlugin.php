<?php

require_once('./HmacSigner.php');

use Http\Client\Common\Plugin;
use Http\Promise\Promise;
use Psr\Http\Message\RequestInterface;

final class BlaizeAuthPlugin implements Plugin {

	private $accessKey;
	private $secretKey;

	function __construct($accessKey, $secretKey) {
		$this->accessKey = $accessKey;
		$this->secretKey = $secretKey;
	}

	public function handleRequest(RequestInterface $request, callable $next, callable $first) : Promise {

		$body = $request->getBody()->getContents();
		$path = preg_replace("|https?://[^/]+/|i", "/", $request->getRequestTarget());
		$path = preg_replace("|\?.*|", "", $path);
		$query = $request->getUri()->getQuery();
		$method = $request->getMethod();
		$timestamp = strval(round(microtime(true)*1000));
		$nonce = uniqid();

		$signer = new HmacSigner("SHA256");
        	$signature = $signer->signRequest($this->secretKey, $body, $path, $query, $method, $timestamp, $nonce);

		$authHeader = "ZEPHR-HMAC-SHA256 $this->accessKey:$timestamp:$nonce:$signature";

		$newRequest = $request->withHeader('Authorization', $authHeader);

		return $next($newRequest);
	}

}
