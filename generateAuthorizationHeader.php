<?php

require_once('HmacSigner.php');

if ($argc != 6) {
	print("Arguments: access secret body path method\n");
	exit(1);
}

$timestamp = strval(round(microtime(true)*1000));
$nonce = uniqid();

try {
	$signer = new HmacSigner("SHA256");
	$signature = $signer->signRequest($argv[2], $argv[3], $argv[4], $argv[5], $timestamp, $nonce);
	print("BLAIZE-HMAC-SHA256 " . $argv[1] . ":" . $timestamp . ":" . $nonce . ":" . $signature . "\n");
} catch (Exception $e) {
	print($e->Message() . "\n");
}
