<?php

require_once('AdminApiClient.php');
require_once('./vendor/autoload.php');

use Http\Discovery\MessageFactoryDiscovery;

$t = AdminApiClient::build("44501ace-c533-4b9e-b4ea-cfa24d18c179", "1a3eadb4-1f54-470f-a0aa-1cf233f65bdd");

$messageFactory = MessageFactoryDiscovery::find();
$response = $t->httpClientPool->sendRequest($messageFactory->createRequest('GET', 'https://demo.admin.blaize.io/v3/users', array("Accept" => "application/json")));

echo $response->getStatusCode();
echo "\n";
echo $response->getBody()->getContents();

$registerRequestBody = array(
	"identifiers" =>
		array("email_address" => "bob@bobberson.bob"),
	"validators" =>
		array("password" => "sup3rS3cr3t!"),
	"attributes" =>
		array("job-title" => "dogsbody", "allow-marketing" => "true", "company" => "Test-co"));
	
$registerRequest = $messageFactory->createRequest('POST', 'https://demo.admin.blaize.io/v3/users', array("Content-Type" => "application/json"), json_encode($registerRequestBody));
$registerResponse = $t->httpClientPool->sendRequest($registerRequest);


echo $registerResponse->getStatusCode();
echo "\n";
echo $registerResponse->getBody()->getContents();
